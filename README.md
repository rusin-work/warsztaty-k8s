# Warsztaty z Kubernetesa

<img src="./docs/doggo.png" alt="drawing" width="200"/>

## Agenda

* Kilka słów teorii
* Instalacja k3d i uruchomienie lokalnego klastra K8S
* Instalacja kubectl
* Barebone K8S  czyli "surowa" praca z kubernetesowymi YAML-ami i kubectl
* Instalacja Lens IDE i podłączenie się do lokalnego klastra
* Instalacja Helm
* Wprowadzenie do Helma i Helm Chartów
* Analiza Helm Charta BFF
* Lokalne uruchomienie Helm Charta BFF
* Praca z Kubernetesowymi sekretami w klastrach Modivo 

## Spis treści

1. [Instalacja k3d i kubectl](docs/kubectl/000-install.md)
1. [Interakcja z kubectl - Pod](docs/kubectl/001-pod.md)
1. [Interakcja z kubectl - ReplicaSet](docs/kubectl/002-replicaset.md)
1. [Interakcja z kubectl - ConfigMap](docs/kubectl/003-configmap.md)
1. [Interakcja z kubectl - Deployment](docs/kubectl/004-deployment.md)
1. [Interakcja z kubectl - Service](docs/kubectl/005-service.md)
1. [Interakcja z kubectl - Ingress](docs/kubectl/006-ingress.md)
1. [Interakcja z kubectl - Secret](docs/kubectl/007-secret.md)
1. Instalacja Lens IDE https://k8slens.dev/download
1. Instalacja Helm https://helm.sh/docs/intro/install
1. [Helm - utworzenie nowego charta](docs/000-helm-example-chart.md)
1. [Lokalne uruchomienie Helm Charta BFF](docs/001-helm-bff-chart.md)