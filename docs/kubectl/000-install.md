# Instalacja `k3d` i `kubectl`

## k3d

1. Zainstaluj k3d - https://k3d.io/v5.6.3/#releases
1. Utwórz lokalny klaster (wymaga zainstalowanego dockera)

```shell
k3d cluster create
```

### Przykładowe polecenia

| Polecenie          | Opis                                       |
|--------------------|--------------------------------------------|
| k3d cluster create | Utworzenie nowego klastra                  |
| k3d cluster stop   | Zatrzymanie klastra                        |
| k3d cluster start  | Uruchomienie utworzonego wcześniej klastra |
| k3d cluster list   | Lista klastrów                             |


## kubectl

1. Zainstaluj kubectl - https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
1. Sprawdź czy kubectl zadziałał z utworzonym klastrem k3d:

```
kubectl get namespaces
```

### Przykładowe polecenia

| Polecenie                            | Opis                                               |
|--------------------------------------|----------------------------------------------------|
| kubectl apply -f file.yaml           | Zaaplikowanie pliku yaml do kubernetesa            |
| kubectl get pods                     | Lista utworzonych podów                            |
| kubectl get deployments              | Lista utworzonych depoymentów                      |
| kubectl get replicasets              | Lista utworzonych replica setów                    |
| kubectl get namespaces               | Lista utworzonych namespace'ów                     |
| kubectl describe pod [nazwa]         | Szczegóły poda wraz z jego aktualnym stanem        |
| kubectl describe deployment [nazwa]  | Szczegóły deploymentu wraz z jego aktualnym stanem |
| kubectl describe replicaset [nazwa]  | Szczegóły replicasetu wraz z jego aktualnym stanem |

Kompletna lista poleceń jest w dokumentacji kubectl: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands


---
[powrót do spisu treści](./)