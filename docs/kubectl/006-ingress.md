# Interakcja z kubectl - Ingress

1. Zaktualizuj serwis:

Zmieniamy typ serwisu z `NodePort` na `ClusterIP`. Dzięki temu na node'ach nie zostanie wystawiony publiczny port, a do podów komunikacja przebiegnie przez kubernetesowy load balancer.

```shell
cat << EOF > service.yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx
spec:
  type: ClusterIP
  ports:
  - port: 80
    targetPort: 80
  selector:
    app: nginx
EOF
```

2. Zaaplikuj YAML przez kubectl:

```shell
kubectl apply -f service.yaml
```

3. Podejrzyj konfigurację serwisu:

```
kubectl describe service nginx
```

4. Utwórz plik z opisem ingressa:

Wyłączamy w metadata `ssl-redirect` - nie chcemy redirectów na HTTPS.

```shell
cat << EOF > ingress.yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-ingress
  annotations:
    ingress.kubernetes.io/ssl-redirect: "false"
spec:
  rules:
  - http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: nginx
            port:
              number: 80
EOF
```

5. Zaaplikuj YAML przez kubectl:

```shell
kubectl apply -f ingress.yaml
```

6. Podejrzyj konfigurację ingressa:

```shell
kubectl describe ingress nginx-ingress
```

6. Wystaw kubernetesowy load balancer na porcie 8081:

```shell
k3d cluster edit k3s-default --port-add "8081:80@loadbalancer"
```

7. Odpytaj kubernetesowy load balancer

```shell
curl localhost:8081
```

Load balancer wskaże na serwis nginx, który obsłuży request przez poda powiązanego z deploymentem nginxa.

Rozszerzmy teraz ingressa o dns i lokalną domenę `k8s-nginx.local`.

9. Zaktualizuj plik z opisem ingressa:

```shell
cat << EOF > ingress.yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-ingress
  annotations:
    ingress.kubernetes.io/ssl-redirect: "false"
spec:
  rules:
  - host: k8s-nginx.local
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: nginx
            port:
              number: 80
EOF
```

10. Zaaplikuj YAML przez kubectl:

```shell
kubectl apply -f ingress.yaml
```

11. Odpytaj kubernetesowy load balancer

```shell
curl localhost:8081
```

404, nie znaleziono usługi, bo host nie został poprawnie zmatchowany.

12. Dodaj wpis w hostsach z lokalną domeną:

```
echo '127.0.0.1  k8s-nginx.local' | sudo tee -a /etc/hosts
```

13. Odpytaj kubernetesowy load balancer przez host:

```shell
curl k8s-nginx.local:8081
```

---
[powrót do spisu treści](./)