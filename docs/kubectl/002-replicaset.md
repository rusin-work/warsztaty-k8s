# Interakcja z kubectl - ReplicaSet

1. Utwórz plik z opisem replica setu:

```shell
cat << EOF > replicaset.yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: bff-busybox-replicaset
spec:
  replicas: 3
  selector:
    matchLabels:
      app: bff
  template:
    metadata:
      labels:
        app: bff
    spec:
        containers:
        - image: busybox
          command:
            - sleep
            - "15"
          imagePullPolicy: IfNotPresent
          name: busybox
        restartPolicy: Always
EOF
```

2. Zaaplikuj YAML przez kubectl:

```shell
kubectl apply -f replicaset.yaml
```

3. Podejrzyj utworzonego replica seta:

```
kubectl get replicaset bff-busybox-replicaset
```

3. Podejrzyj utworzone pody:

```
kubectl get pods
```

5. Usuń replica set

```
kubectl delete replicaset bff-busybox-replicaset
```

---
[powrót do spisu treści](./)