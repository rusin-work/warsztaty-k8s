# Interakcja z kubectl - Service

1. Utwórz plik z opisem serwisu:

```shell
cat << EOF > service.yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx
spec:
  type: NodePort
  ports:
  - port: 80
    targetPort: 80
    nodePort: 30001
  selector:
    app: nginx
EOF
```

2. Zaaplikuj YAML przez kubectl:

```shell
kubectl apply -f service.yaml
```

3. Podejrzyj konfigurację serwisu:

```
kubectl describe service nginx
```

4. Ekspozycja portu `30001` z klastra jako `8082`

```shell
k3d cluster edit k3s-default --port-add "8082:30001@server:0"
```

5. Odpytaj klaster o serwis

```
curl localhost:8082
```

### Kilka słów o `NodePort`


W K8S ekspozycja przez `NodePort` dotyka wszystkich node'ów, czyli przy 10 replikach zaplanowanych przez scheduler na 10 serwerach każdy z nich udostępni publicznie port 30001, na którym będzie można się dobić do usługi nginxa.

Znając adresy IP node'ów można się skomunikować bezpośrednio z serwisem, z pominięciem load balancera, np. przez `curl 192.168.0.1:30001`

Odpowiednikiem tego podejścia w dockerze jest ekspozycja portu - https://docs.docker.com/guides/docker-concepts/running-containers/publishing-ports

Taka konfiguracja nie jest sugerowana pod produkcyjne klastry. 

Konfigurację "po bożemu", czyli przez Ingressa z ruchem kierowanym do usługi przez load balancer zrobimy w kolejnym kroku.

---
Dokumentacja: https://kubernetes.io/docs/tutorials/kubernetes-basics/expose/expose-intro/

[powrót do spisu treści](./)