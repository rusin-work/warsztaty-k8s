# Interakcja z kubectl - Deployment

1. Utwórz plik z opisem deploymentu:

```shell
cat << EOF > deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  selector:
    matchLabels:
      app: nginx
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx:latest
        name: nginx
        ports:
        - containerPort: 80
          name: web
        volumeMounts:
          - name: config-vol
            mountPath: /etc/nginx/
      volumes:
        - name: config-vol
          configMap:
            name: nginx-config
            items:
              - key: nginx.conf
                path: nginx.conf
EOF
```

2. Zaaplikuj YAML przez kubectl:

```shell
kubectl apply -f deployment.yaml
```

3. Obserwuj postęp:

```shell
kubectl get deployment nginx
kubectl get pods
```

---
[powrót do spisu treści](./)