# Interakcja z kubectl - Secret

1. Utwórz plik z opisem secretu:

```shell
cat << EOF > secret.yaml
apiVersion: v1
kind: Secret
metadata:
  name: access-credentials
type: Opaque
data:
  username: $(echo -n "admin" | base64)
  password: $(echo -n "admin" | base64)
EOF
```

2. Zaaplikuj YAML przez kubectl:

```shell
kubectl apply -f secret.yaml
```