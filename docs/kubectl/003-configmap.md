# Interakcja z kubectl - ConfigMap

1. Utwórz plik z opisem config mapy:

```shell
cat << EOF > configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-config
data:
  nginx.conf: '
events {
}
http {
   server {
       listen 80;
       location / {
           return 200 "Hello world!";
       }
   }
}
'
EOF
```

2. Zaaplikuj YAML przez kubectl:

```shell
kubectl apply -f configmap.yaml
```

3. Podejrzyj utworzoną config mapę:

```
kubectl get configmap nginx-config
kubectl get configmap nginx-config  -o "jsonpath={.data['nginx\.conf']}"
```

4. Zaktualizuj config mapę o konfigurację multiline:

```shell
cat << EOF > configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-config
data:
  nginx.conf: |-
    events {
    }
    http {
       server {
           listen 80;
           location / {
               return 200 "Hello world!";
           }
       }
    }
EOF
```

5. Zaaplikuj YAML przez kubectl:

```shell
kubectl apply -f configmap.yaml
```

6. Podejrzyj utworzoną config mapę:

```
kubectl get configmap nginx-config  -o "jsonpath={.data['nginx\.conf']}"
```

---
[powrót do spisu treści](./)