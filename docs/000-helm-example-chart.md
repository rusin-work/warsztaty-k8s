# Helm - utworzenie nowego charta

1. Usuń wszystkie wcześniej utworzone zasoby:

```shell
kubectl delete configmap nginx-config
kubectl delete deployment nginx
kubectl delete service nginx
kubectl delete ingress nginx-ingress
```

2. Utwórz nowy katalog:

```shell
mkdir nginx-hello-world
cd nginx-hello-world
```

3. Utwórz Chart.yaml

```shell

cat << EOF > Chart.yaml
apiVersion: v2
name: nginx-example
description: Nginx hello world helm chart
type: application
version: 0.1.0
appVersion: "0.1.0"
EOF
```

4. Utwórz katalog `templates`

```shell
mkdir templates
```

5. Utwórz kubernetesowe zasoby z poprzednich ćwiczeń:

```shell
cat << EOF > templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-config
data:
  nginx.conf: |-
    events {
    }
    http {
       server {
           listen 80;
           location / {
               return 200 "Hello world!";
           }
       }
    }
EOF

cat << EOF > templates/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-hello-world
spec:
  selector:
    matchLabels:
      app: nginx-hello-world
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: nginx-hello-world
    spec:
      containers:
        - image: nginx:latest
          name: nginx
          ports:
            - containerPort: 80
              name: web
          volumeMounts:
            - name: config-vol
              mountPath: /etc/nginx/
      volumes:
        - name: config-vol
          configMap:
            name: nginx-config
            items:
              - key: nginx.conf
                path: nginx.conf
EOF

cat << EOF > templates/ingress.yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-hello-world-ingress
  annotations:
    ingress.kubernetes.io/ssl-redirect: "false"
spec:
  rules:
    - host: k8s-nginx.local
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: nginx-hello-world-service
                port:
                  number: 80
EOF

cat << EOF > templates/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-hello-world-service
spec:
  type: ClusterIP
  ports:
    - port: 80
      targetPort: 80
  selector:
    app: nginx-hello-world
EOF
```

6. Zainstaluj charta

```shell
helm upgrade --install nginx-hello-world .
```

7. Odpytaj kubernetesowy load balancer przez host:

```shell
curl k8s-nginx.local:8081
```

8. Podmiana `nginx-hello-world` na wartości z template:

```shell
sed -i 's/nginx-hello-world/{{ .Values.app.name }}/' templates/configmap.yaml
sed -i 's/nginx-hello-world/{{ .Values.app.name }}/' templates/deployment.yaml
sed -i 's/nginx-hello-world/{{ .Values.app.name }}/' templates/ingress.yaml
sed -i 's/nginx-hello-world/{{ .Values.app.name }}/' templates/service.yaml
```

9. Nowy plik values:

```shell
cat << EOF > values.yaml
app:
  name: nginx-hello-world
EOF
```

10. Podejrzyj template z wartościami. YAML w takiej postaci zostanie przekazany do `kubectl apply`.

```shell
helm template -f values.yaml ngix-hello-world .
```

11. Zaktualizuj charta z wykorzystaniem pliku values:

```shell
helm upgrade --install -f values.yaml nginx-hello-world .
```

---
[powrót do spisu treści](./)