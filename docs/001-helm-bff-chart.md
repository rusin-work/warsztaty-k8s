# Lokalne uruchomienie Helm Charta BFF

1. Ustaw w zmiennych swój GitLabowy username i personal access token

```
GITLAB_USERNAME=<>
GITLAB_TOKEN=<>
```

2. Nadaj uprawnienia Helmowi do pobierania organizacyjnych chartów

```
helm repo add \
--username $GITLAB_USERNAME \
--password $GITLAB_TOKEN \
modivo-helm-charts \
https://gitlab.com/api/v4/projects/43329722/packages/helm/stable
```

3. Nadaj uprawnienia Kubernetesowi do pobierania organizacyjnych obrazów

```
kubectl create namespace bff
kubectl create secret docker-registry gitlab-registry \
--namespace bff \
--docker-server=registry.gitlab.com \
--docker-username=$GITLAB_USERNAME \
--docker-password=$GITLAB_TOKEN
```

4. Zainstaluj stack Prometheusa

```shell
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm upgrade --install --wait --namespace=prometheus --create-namespace \
               --set grafana.enabled=false \
               --set alertmanager.enabled=false \
               --set nodeExporter.enabled=false \
               --set prometheus-node-exporter.prometheus.monitor.enabled=false \
               --set defaultRules.create=false \
               kube-prometheus-stack \
               prometheus-community/kube-prometheus-stack
```

5. Sklonuj repozytorium k8s-deployments

```
git clone https://gitlab.com/eobuwie/infra/k8s-deployments
```

6. Przejdź do charta BFF i zaktualizuj zależności

```
cd k8s-deployments/charts/bff
helm dependency update
```

7. Uruchom chart BFF-a

```shell
helm upgrade --namespace bff \
--install \
-f values.yaml \
-f values.local.yaml \
modivo-bff .
```

8. Obserwuj w Lensie postęp
9. Po powodzeniu instalacji wystaw port forward do serwisu bffa
10. Wejdź przez przeglądarkę na adres + forwardnięty port
11. Jeżeli zobaczysz index page z projektu to gratulacje, właśnie postawiłeś lokalnie BFF przez Kubernetesa

---
[powrót do spisu treści](./)